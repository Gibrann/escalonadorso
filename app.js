angular.module("app", ["ui.router"])

.config(function($stateProvider, $urlRouterProvider){


  $stateProvider

  .state('index', {
        url: '/index',
        templateUrl: 'templates/main.html',
        controller: 'Main'
  })

  .state('ltg_best_fit', {
        url: '/lgt_best_fit',
        params: {param: null},
        templateUrl: 'templates/least_time_to_go.html',
        controller: 'LTGControllerBF as vm'
  })

  .state('ltg_quick_fit', {
        url: '/lgt_quick_fit',
        params: {param: null},
        templateUrl: 'templates/least_time_to_go.html',
        controller: 'LTGControllerQF as vm'
  })

  .state('ltg_merge_fit', {
      url: '/lgt_merge_fit',
      params: {param: null},
      templateUrl: 'templates/least_time_to_go.html',
      controller: 'LTGControllerMF as vm'
  })

  .state('round_robin_best_fit', {
        url: '/round_robin_best_fit',
        params: {param: null},
        templateUrl: 'templates/round_robin.html',
        controller: 'RRControllerBF as vm'
  })

  .state('round_robin_quick_fit', {
        url: '/round_robin_quick_fit',
        params: {param: null},
        templateUrl: 'templates/round_robin.html',
        controller: 'RRControllerQF as vm'
  })

  .state('round_robin_merge_fit', {
        url: '/round_robin_merge_fit',
        params: {param: null},
        templateUrl: 'templates/round_robin.html',
        controller: 'RRControllerMF as vm'
  })

  .state('interval_scheduling_best_fit', {
        url: '/interval_scheduling_best_fit',
        params: {param: null},
        templateUrl: 'templates/interval-scheduling.html',
        controller: 'IBSControllerBF as vm'
  })

  .state('interval_scheduling_quick_fit', {
      url: '/interval_scheduling_quick_fit',
      params: {param: null},
      templateUrl: 'templates/interval-scheduling.html',
      controller: 'IBSControllerQF as vm'
  })

  .state('interval_scheduling_merge_fit', {
      url: '/interval_scheduling_merge_fit',
      params: {param: null},
      templateUrl: 'templates/interval-scheduling.html',
      controller: 'IBSControllerMF as vm'
  })

  .state('rr_no_priority', {
        url: '/rr_no_priority',
        params: {param: null},
        templateUrl: 'templates/rr_no_priority.html',
        controller: 'RRNoPriorityController as vm'
  });

  $urlRouterProvider.otherwise('/index');

});
