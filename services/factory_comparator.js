/**
 * Created by mac-gibran on 08/04/16.
 */

(function () {

    angular.module("app").factory("Comparator", Comparator);

    function Comparator() {

        var service = {};

        service = {
            isListEquals: isListEquals
        };
        
        return service;
        
        function isListEquals(list1, list2) {
            
            if(list1.length != list2.length){
                return false;
            }
            
            for(var i=0; i < list1.length; i++){

                if(list1[i].id != list2[i].id) {
                    return false;
                }
            }
            
            return true;
        }
        

    }

})();
