/**
 * Created by mac-gibran on 15/05/16.
 */

(function () {

    angular.module("app").factory("QuickFitService", QuickFitService);

    QuickFitService.$inject = ["$q", "FilterManager"];

    function QuickFitService($q, FilterManager) {

        var aux = {};

        var service = {
            initialize: initialize,
            countOftenBloks: countOftenBloks,
            findFreeBlock: findFreeBlock,
            getCurrentOftenBlocks: getCurrentOftenBlocks,
            incrementRequestCount: incrementRequestCount,
            getCountRequest: getCountRequest
        };

        aux.useOftenBlocks = false;
        aux.currentOftenBlocks = Object.create(null);
        aux.defaultIldleColor = "ui red message";
        aux.defaultRunnigColor = "ui green message";
        aux.index = 0;
        aux.indice = 0;

        return service;

        function initialize() {
            aux.allOftenBlocks = Object.create(null);
            aux.countRequests = 0;
            aux.oftensBlocks = Object.create(null);
            aux.keepPositionsBlock = Object.create(null);
        }

        function countOftenBloks(block, vetor) {
            var size = block.size;
            if(size in aux.allOftenBlocks){
                aux.allOftenBlocks[size]++;
            }else{
                aux.allOftenBlocks[size] = 1;
                aux.oftensBlocks[size] = [];
            }
            //oftenBlocks - guarda os blocos mais acessados pela seu tamanho;
            if(!(block.id in aux.keepPositionsBlock)) {
                aux.keepPositionsBlock[block.id] = 0;
                aux.oftensBlocks[size].push(vetor[block.position]);
            }
        }

        function getCountRequest() {
            return aux.countRequests;
        }

        function incrementRequestCount() {
            aux.countRequests++;
        }
        
        function getCurrentOftenBlocks() {
            return Object.keys(aux.currentOftenBlocks).join();
        }

        function setCurrentOftenBlocks(qtd, memoria) {
            return $q(function (resolve, reject) {

                angular.forEach(memoria, function (bloco) {
                    if(bloco.estado == 0){
                        bloco.bg = "ui red message";
                    }
                    bloco.often = false;
                });

                var vetor = Object.keys(aux.allOftenBlocks).sort(function(a,b){return aux.allOftenBlocks[b]-aux.allOftenBlocks[a]});
                vetor = FilterManager.limitTo(vetor,qtd);
                for(var i = 0; i < vetor.length; i++){
                    aux.index = parseInt(vetor[i]);
                    for(var j = 0; j < aux.oftensBlocks[aux.index].length; j++){
                        var element = aux.oftensBlocks[aux.index][j];
                        element.often = true;
                        if(element != undefined && element.estado == 0) {
                            element.bg = "color" + element.size;
                        }
                    }
                }
                aux.currentOftenBlocks = Object.create(null);
                angular.forEach(vetor, function (v,k) {
                    aux.currentOftenBlocks[v] = aux.oftensBlocks[v];
                    if(vetor.length-1 == k) {
                        resolve();
                    }
                });
            });
        }
        
        function hasFreeBlock(key) {
            var availableKey = undefined;
            var keyFound = false;
            angular.forEach(aux.currentOftenBlocks[key], function (v,k) {
                if(!keyFound && v.estado == 0){
                    availableKey = k;
                    keyFound = true;
                }
            });
            return availableKey;
        }

        function findFreeBlock(vetor, processo, allBlocks) {
            var promise = undefined;
            if(aux.countRequests >= 20){
                promise = setCurrentOftenBlocks(6, allBlocks);
                aux.useOftenBlocks = true;
            }

            if(promise != undefined){
                promise.then(function () {
                    initialize();
                    return freeBlock(vetor, processo);
                }, function () {});
            }else{
                    return freeBlock(vetor, processo);
            }

        }

        function freeBlock(vetor, processo) {
            var freeBlock = undefined;
            if(aux.useOftenBlocks) {
                var freeBlockKey = hasFreeBlock(processo.size);
            }
            if(freeBlockKey != undefined && processo.size in aux.currentOftenBlocks){
                freeBlock =  vetor[freeBlockKey];
            }else{
                var located = false;
                angular.forEach(vetor, function (v,k) {
                    if(!located && v.size >= processo.size){
                        freeBlock = vetor[k];
                        located = true;
                    }
                });
            }
            incrementRequestCount();
            return freeBlock;
        }


    }


})();