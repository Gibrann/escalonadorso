(function () {

  angular.module("app").factory("FactoryProcesso", FactoryProcesso);

  function FactoryProcesso(){

    var aux = {};
    aux.cont = 0;
    aux.contPrioridade = 0;

    var service = {
        buildProcessosRR: buildProcessosRR,
        buildProcessosLTG: buildProcessosLTG,
        novoProcessoRR: novoProcessoRR,
        novoProcessoLTG: novoProcessoLTG,
        novoProcessoIBS: novoProcessoIBS,
        buildProcessosIBS: buildProcessosIBS,
        inverterInvervalos: inverterInvervalos,
        novoProcessoDinamicoIBS: novoProcessoDinamicoIBS
    };

    return service;


    function getRandomRange(min, max){
      return Math.floor((Math.random() * max) + min);
    }

    function gerarIntervalosProcessoDinamicoIBS(processo, range){
      var inicio = 0;
      var final = 0;
      var keepGoing = true;

      while(keepGoing){
          inicio = getRandomRange(range,range);
          final = getRandomRange(range,range);
          if(inicio <  final)
              keepGoing = false;
      }

      processo.inicio = inicio;
      processo.final = final;

      return processo;
    };

    function gerarIntervalosProcesso(processo){
      var inicio = 0;
      var final = 0;
      var keepGoing = true;

      while(keepGoing){
          inicio = getRandomRange(1,21);
          final = getRandomRange(1,21);
          if(inicio <  final)
              keepGoing = false;
      }

      processo.inicio = inicio;
      processo.final = final;

      return processo;
    };

    function inverterInvervalos(vetor) {
      var start = 0;
      var end = 0;
      angular.forEach(vetor, function(v,k){
          if(v) {
              start = v.inicio;
              end = v.final;
              v.inicio = end;
              v.final = start;
          }else{
              vetor.splice(k,1);
          }
      });
    };

    function buildProcessosRR(tamanho){
      var processos = [];
      for (var i = 0; i < tamanho; i++) {
        processos.push(novoProcessoRR());
      }
      return processos;
    }

    function buildProcessosLTG(tamanho){
      var processos = [];
      for (var i = 0; i < tamanho; i++) {
          processos.push(novoProcessoLTG());
      }
      return processos;
    }

    function buildProcessosIBS(tamanho){
      var processos = [];
      for (var i = 0; i < tamanho; i++) {
          processos.push(novoProcessoIBS());
      }
      return processos;
    };

    function standartProcesso() {
      var processo = {};

      processo.name = "p"+aux.cont;

      processo.block = [];

      processo.estado = 0;

      processo.id = aux.cont;

       var range =  getRandomRange(2,128);

      if(range > 128){
          processo.size = 128;
      }else{
          processo.size = range;
      }

      aux.cont++;

      return processo;
    }

    function novoProcessoIBS() {

      var processo = standartProcesso();

      processo = gerarIntervalosProcesso(processo);

      processo.tempoDuracao = processo.final - processo.inicio;

      return processo;

    };

    function novoProcessoDinamicoIBS(plus) {

      var processo = standartProcesso();

      processo = gerarIntervalosProcessoDinamicoIBS(processo,plus);

      processo.bg = "ui blue message";

      processo.inicio += plus;
      processo.final += plus;

      processo.tempoDuracao = processo.final - processo.inicio;

      return processo;

    };

    function novoProcessoRR() {

      var processo = standartProcesso();

      processo.tempoDuracao = getRandomRange(17,4);

      processo.prioridade = aux.contPrioridade;

      processo.alocado = false;

      if(++aux.contPrioridade == 4)
            aux.contPrioridade = 0

      return processo;

    }

    function novoProcessoLTG() {

      var processo = standartProcesso();

      var rangeTD = getRandomRange(0,17);
      var rangeDL = getRandomRange(0,17);


      if(rangeTD < 4) rangeTD += 4;
      if(rangeDL < 4) rangeDL += 4;

      processo.tempoDuracao = rangeTD;

      processo.deadLine = rangeDL;

      return processo;

    }

  };


})();