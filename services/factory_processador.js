angular.module("app").factory("FactoryProcessador", function(){


  var aux = {};
  aux.cont = 0;
  var service = {
      buildProcessadores: buildProcessadores,
      novoProcessador: novoProcessador,
      novoProcessadorIBS: novoProcessadorIBS,
      buildProcessadoresIBS: buildProcessadoresIBS
  };

  return service;

  function buildProcessadores(tamanho){
    var processos = [];
    for (var i = 0; i < tamanho; i++) {
        processos.push(service.novoProcessador());
    }
    return processos;
  }

  function buildProcessadoresIBS(tamanho){
     var processos = [];
     for (var i = 0; i < tamanho; i++) {
           processos.push(service.novoProcessadorIBS());
     }
     return processos;
  }

  function novoProcessadorIBS() {

      var processador = {};

      processador = getStandardProcessador();

      processador.processos = [];

      return processador;
  }


  function novoProcessador(){
     return getStandardProcessador();
  }

  function getStandardProcessador() {
      var processador = {};

      processador.bg = "ui red message";
      processador.nome = "Processador "+aux.cont;
      processador.core = -1;
      processador.estado = 0;
      aux.cont++;

      return processador;
  }

});
