/**
 * Created by mac-gibran on 01/05/16.
 */

(function () {

    angular.module("app").factory("FactoryBestFit", FactoryBestFit);

    FactoryBestFit.$inject = ["$filter"];

    function FactoryBestFit($filter) {

        var service = {
            findBestFit: findBestFit,
            initialize: initialize
        };

        var qtdFitBlock = 1;
        var aux = this;
        aux.resultado = [];

        return service;

        function initialize(){
            aux.resultado = [];
        }

        function findArray(array,sizeProcesso) {

            var size = array.length;
            var novoArray = array;

            while(size > 1) {

                var r = Math.floor(size / 2);

                var element1 = novoArray[r-1].size;

                if (element1 >= sizeProcesso) {
                    novoArray = $filter("limitTo")(novoArray, r);
                } else {
                    novoArray = $filter("limitTo")(novoArray, novoArray.length, r);
                }

                size = novoArray.length;
            }

            return novoArray;

        }

        function findBestFit(array,sizeProcesso) {

            var sum = 0;

            var novoArray = findArray(array, sizeProcesso);

            novoArray = $filter("orderBy")(novoArray, "position");

            angular.forEach(novoArray, function (v) {
                if(v != undefined)
                    sum += v.size;
            });

            if(sum >= sizeProcesso){
                aux.resultado = novoArray;
            }

            return aux.resultado;
        }

    }

})();