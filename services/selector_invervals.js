(function(){

  angular.module("app").factory("SelectorInverval", SelectorInverval);

  function SelectorInverval(){
    
    var service = {
      selectRunningInterval: selectRunningInterval
    };
    return service;

    function selectRunningInterval(vetor) {
      var retorno = [];
      var running = [];
      var remainder = [];
      var last_end = -1;
      var aux = {};
      aux.novoInicio = "";
      angular.forEach(vetor, function(v){
          if(v) {
              var start = v.final;
              var end = v.inicio;
              v.final = end;
              v.inicio = start;
              if (start >= last_end) {
                  last_end = end;
                  running.push(v);
              } else {
                  remainder.push(v);
              }
          }else{
              vetor.splice(k,1);
          }
      });
      retorno.push(running);
      retorno.push(remainder);
      return retorno;
    }

  }

})();
