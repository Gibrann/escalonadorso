(function () {

  angular.module("app").service("CoreManager", CoreManager);

  function CoreManager(){

    var servico = {
      exitsCoreEmpty: exitsCoreEmpty
    }

    return servico;

    function exitsCoreEmpty(qtd_core_existentes, qtd_core_parado){
      return (qtd_core_existentes > qtd_core_parado);
    }
    

  };


})();