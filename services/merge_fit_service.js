/**
 * Created by mac-gibran on 11/05/16.
 */

(function () {

    angular.module("app").factory("MergeFitService", MergeFitService);

    MergeFitService.$inject = ["$q", "FactoryBlock"];

    function MergeFitService($q, FactoryBlock) {

        var service = {
            mergeFreeBlocks: mergeFreeBlocks,
            findBlockAvailabe: findBlockAvailabe,
            desalocarBloco: desalocarBloco
        };

        return service;

        function mergeFreeBlocks(blocks){
            return $q(function (resolve, reject) {
                angular.forEach(blocks, function (value,key) {
                    if(key > 0 && key <= blocks.length-2 && blocks[key].estado == 0){
                        if(blocks[key-1].estado == 0) {
                            blocks[key - 1].size += blocks[key].size;
                            blocks.splice(key, 1);
                        }
                        if(blocks[key+1] != undefined && blocks[key+1].estado == 0){
                            blocks[key].size += blocks[key+1].size;
                            blocks.splice(key+1, 1);
                        }
                    }
                    if(value.processo.id == undefined) {
                        value.estado = 0;
                        value.bg = "ui red message";
                        value.processo = {};
                    }
                    if(blocks.length == 2 && blocks[0].estado == 0 && blocks[1].estado == 0){
                        blocks[0].size += blocks[1].size;
                        blocks.splice(1,1);
                    }
                    if(key == blocks.length-1)
                        resolve();
                });
            });
        }

        function desalocarBloco(blocks, obj) {
            return $q(function (resolve,reject) {
                angular.forEach(blocks, function (value, key) {
                        if(value.processo.id == obj.id || value.processo.id == undefined) {
                            value.estado = 0;
                            value.bg = "ui red message";
                            value.processo = {};
                        }
                        if(key == blocks.length-1)
                            resolve();
                });
            });
        }

        function findBlockAvailabe(blocks, sizeProcesso) {
            var located = false;
            var result = [];
            angular.forEach(blocks, function (value,key) {
                if(value.estado == 0 && value.size >= sizeProcesso){
                    if(value.size > sizeProcesso) {
                        var newBlock = FactoryBlock.getNewBlock();
                        newBlock.size = value.size - sizeProcesso;
                        value.size = sizeProcesso;
                        blocks.push(newBlock);
                    }
                    result[2] = key;
                    value.bg = "ui green message";
                    value.estado = 1;
                    located = true;
                }
            });
            result[0] = located;
            return result;
        }

    }

})();


