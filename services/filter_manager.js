(function () {

  angular.module("app").service("FilterManager", FilterManager);

  FilterManager.$inject = ["$filter"];

  function FilterManager($filter){

    var service = {
      orderByDeadLine: orderByDeadLine,
      orderByTempoDrc: orderByTempoDrc,
      orderByQuantum: orderByQuantum,
      limitTo: limitTo,
      orderByInicio: orderByInicio,
      orderBy: orderBy
    }

    return service;

    function orderByDeadLine(vetor){
      return $filter("orderBy")(vetor, 'deadLine');
    }

    function orderByTempoDrc(vetor){
      return $filter("orderBy")(vetor, 'processo.tempoDuracao');
    }

    function orderByQuantum(vetor){
      return $filter("orderBy")(vetor, 'quantum');
    }

    function limitTo(vetor, index){
      return $filter("limitTo")(vetor, index);
    }

    function orderByInicio(vetor){
      return $filter("orderBy")(vetor, "inicio");
    }

    function orderBy(vetor, param) {
      return $filter("orderBy")(vetor,param);
    }

  };


})();