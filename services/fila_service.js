/**
 * Created by mac-gibran on 12/05/16.
 */

(function () {
    
    angular.module("app").factory("Fila", Fila);
    
    function Fila() {

        var fila = {
            enfileirar: enfileirar,
            desenfileirar: desenfileirar,
            isEmpty: isEmpty
        };

        var vetorFila = [];

        return fila;

        function enfileirar(obj) {
            vetorFila.push(obj);
        }

        function desenfileirar() {
            var obj = undefined;
            if(!isEmpty()) {
                obj = vetorFila[0];
                vetorFila.splice(0,1);
            }
            return obj;
        }

        function isEmpty() {
            return (vetorFila.length == 0);
        }

    }
    
    
})();