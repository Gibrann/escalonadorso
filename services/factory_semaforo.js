(function () {

    "use strict";

    angular.module("app").factory("ServiceSemaforo", ServiceSemaforo);

    function ServiceSemaforo() {

        var aux = [];

        var service = {
            buildArrayOfKeys: buildArrayOfKeys,
            getAvailableKey: getAvailableKey,
            getKeyCoresArray: getKeyCoresArray,
            getKeyCoresArrayLTG: getKeyCoresArrayLTG,
            initialize: initialize,
            decrementUnavailableCores: decrementUnavailableCores,
            incrementUnavailableCores: incrementUnavailableCores,
            getQtdUnavailableCores: getQtdUnavailableCores,
            pushKey: pushKey,
            removeKey: removeKey
        };

        return service;

        function initialize() {
            aux.keysOfEmptyCores = [];
            aux.qtdUnavailableCores = 0;
        }

        function decrementUnavailableCores() {
            aux.qtdUnavailableCores--;
        }

        function incrementUnavailableCores() {
            aux.qtdUnavailableCores++;
        }
        
        function getQtdUnavailableCores() {
            return aux.qtdUnavailableCores;
        }

        function buildArrayOfKeys(size) {
            for(var i=0;i<size;i++){
                aux.keysOfEmptyCores.push(i);
            }
        }

        function pushKey(key) {
            aux.keysOfEmptyCores.push(key);
        }

        function getAvailableKey(){
            var key = aux.keysOfEmptyCores[0];
            return key;
        }

        function getKeyCoresArray(){
            var index = undefined;
            if(aux.keysOfEmptyCores.length > 0){
                index = aux.keysOfEmptyCores[0];
            }
            return index;
        }
        
        function removeKey(key) {
            angular.forEach(aux.keysOfEmptyCores, function (v,k) {
               if(key == v){
                   aux.keysOfEmptyCores.splice(k,1);
               }
            });
        }

        function getKeyCoresArrayLTG(hasAptos){
            var index = undefined;
            if(aux.keysOfEmptyCores.length > 0 && hasAptos){
                index = aux.keysOfEmptyCores[0];
            }
            return index;
        }

    }

})();