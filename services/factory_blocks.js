/**
 * Created by mac-gibran on 30/04/16.
 */

(function () {

    angular.module("app").factory("FactoryBlock", FactoryBlock);
    
    function FactoryBlock() {

        var service = {
            getNewBlock: getNewBlock,
            addKey: addKey,
            removeKey: removeKey,
            getAllAvailableKeys: getAllAvailableKeys,
            incrementAvailableKey: incrementAvailableKey,
            decrementAvailableKey: decrementAvailableKey,
            isEmpty: isEmpty,
            initialize: initialize
        }

        var aux = {};
        aux.id = 0;
        aux.pos = 0;
        aux.freeBlocksKeys = [];
        aux.qtdAvailableKey = 0;
        
        return service;

        function initialize() {
            aux.id = 0;
            aux.pos = 0;
            aux.freeBlocksKeys = [];
            aux.qtdAvailableKey = 0;
        }
        
        function getNewBlock() {

            var block = {};

            block.size = 0;
            block.estado = 0;
            block.position = aux.pos;
            block.bg = "ui red message";
            block.processo = {};
            block.id = aux.id;
            block.often = false;
            
            aux.id++;
            aux.pos++;

            return block;

        }
        
        function addKey(key) {
            aux.freeBlocksKeys.push(key);
        }

        function incrementAvailableKey() {
            aux.qtdAvailableKey++;
        }

        function decrementAvailableKey() {
            aux.qtdAvailableKey--;
        }

        function removeKey(key) {
            angular.forEach(aux.freeBlocksKeys, function (v,k) {
                if(v == key) {
                    aux.freeBlocksKeys.splice(k, 1);
                    decrementAvailableKey();
                }
            });
        }

        function getAllAvailableKeys() {
            var clone = [];
            angular.forEach(aux.freeBlocksKeys, function (v) {
               clone.push(v);
            });
            return clone;
        }

        function isEmpty() {
            return (aux.freeBlocksKeys.length == 0);
        }

    }

})();
