(function(){

  angular.module("app").filter("pad", pad);

  function pad() {
    return function(num) {
      return (num < 10 ? '0' + num : num); // coloca o zero na frente
    }
  }

})();
