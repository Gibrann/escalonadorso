(function () {

    angular.module("app").directive("listaPrioridade", listaPrioridades);

    function listaPrioridades() {
        return {
            templateUrl: "directives/lista_prioridades_template.html",
            restrict: "E",
            scope: {
                processos: "=",
                prioridade: "@"
            }
        };
    }

})();

