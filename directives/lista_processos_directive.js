(function () {

    angular.module("app").directive("tableProcessos", tableFinalizados);
    
    function tableFinalizados() {
        return {
            templateUrl: "directives/lista_processos_template.html",
            restrict: "E",
            scope: {
                processos: "=",
                title: "@"
            }
        };
    }
    
})();

