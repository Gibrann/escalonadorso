/**
 * Created by mac-gibran on 08/05/16.
 */

(function () {

    angular.module("app").directive("listaMemoria", ListaMemoria);

    function ListaMemoria() {

        return {
            templateUrl: "directives/memoria_template.html",
            restrict: "E",
            scope: {
                memoria: "=",
                current: "=",
                total: "="
            }
        };

    }

})();
