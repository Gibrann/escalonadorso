/**
 * Created by mac-gibran on 11/05/16.
 */

(function () {

    "use strict";

    angular.module("app").controller("LTGControllerMF", LTGControllerMF);

    LTGControllerMF.$inject = ["Fila", "MergeFitService", "$scope", "$stateParams", "$interval", "CoreManager", "FilterManager", "FactoryProcesso", "FactoryProcessador", "ServiceSemaforo", "FactoryBlock"];

    function LTGControllerMF(Fila, MergeFitService, $scope, $stateParams, $interval, CoreManager, FilterManager, FactoryProcesso, FactoryProcessador, ServiceSemaforo, FactoryBlock) {


        var vm = this;
        vm.filaDeAptos = [];
        vm.cores = [];
        vm.filaAbortados = [];
        vm.processosFinalizados = [];
        vm.memory = [];
        var aux = {};
        aux.filaDeProcesso = [];
        aux.isStopped = true;

        if($stateParams.param){
            vm.data = $stateParams.param.data;
            vm.data.tamanho_memoria = parseInt(vm.data.tamanho_memoria);
            var block = FactoryBlock.getNewBlock();
            block.size = vm.data.tamanho_memoria;
            vm.memory.push(block);
            ServiceSemaforo.initialize();
            ServiceSemaforo.buildArrayOfKeys(vm.data.qtd_core);
            setFilaAptos();
        }

        $scope.$on("checkCores", function (){
            if(CoreManager.exitsCoreEmpty(vm.data.qtd_core, ServiceSemaforo.getQtdUnavailableCores())){
                escalonar();
            }
        });

        function ordenarFilaAptos() {
            vm.filaDeAptos = FilterManager.orderByDeadLine(vm.filaDeAptos);
        }

        function setFilaAptos(){
            vm.filaDeAptos = FactoryProcesso.buildProcessosLTG(vm.data.qtd_processo);
            run();
        }

        function getElementIndex(obj){
            var key = -1;
            angular.forEach(vm.cores, function(v,k){
                if(v.processo != undefined && obj.name == v.processo.name){
                    key = k;
                }
            });
            return key;
        }

        function stopTimerProcesso(processo) {
            if(processo.timer) $interval.cancel(processo.timer);
        }

        function standartSchedule(k, obj) {
            if(k == undefined) return;
            var result = MergeFitService.findBlockAvailabe(vm.memory, obj.size);
            if(result[0]){
                var index = result[2];
                ServiceSemaforo.removeKey(k);
                ServiceSemaforo.incrementUnavailableCores();
                obj.estado = 1;
                vm.memory[index].processo = obj;
                obj.block = index;
                vm.cores[k].processo = obj;
                vm.cores[k].cor = "green message";
                threadCores(obj);
            }else{
                obj.estado = 3;
                vm.filaAbortados.push(obj);
                $scope.$emit("checkCores");
            }
        }

        function escalonar(){
            var k = ServiceSemaforo.getKeyCoresArrayLTG(vm.filaDeAptos.length > 0);
            var obj = getFirstApto();
            if(obj == undefined) return;
            vm.filaDeAptos.splice(0,1);
            stopTimerProcesso(obj);
            standartSchedule(k,obj);
        }

        function getFirstApto() {
            return vm.filaDeAptos[0];
        }

        function removeObjCore(obj){
            var k = getElementIndex(obj);
            aux.filaDeProcesso.push(obj);
            if(k != -1){
                ServiceSemaforo.decrementUnavailableCores();
                vm.processosFinalizados.push(vm.cores[k].processo);
                vm.cores[k].processo = {};
                vm.cores[k].cor = "red message";
                ServiceSemaforo.pushKey(k);
                var promiseDesalocar = MergeFitService.desalocarBloco(vm.memory, obj);
                promiseDesalocar.then(function () {
                    var promiseMerge = MergeFitService.mergeFreeBlocks(vm.memory);
                    promiseMerge.then(function () {
                        $scope.$emit("checkCores");
                    }, function () {});
                }, function () {});
            }
        }

        function despacharProcesso() {
            var timer = $interval(function () {
                if(Fila.isEmpty()){
                    $interval.cancel(timer);
                    aux.isStopped = true;
                }else {
                    var obj = Fila.desenfileirar();
                    removeObjCore(obj);
                }
            },100);
        }

        function threadCores(obj) {
            var timer = $interval(function () {
                if(obj.tempoDuracao == 0){
                    $interval.cancel(timer);
                    Fila.enfileirar(obj);
                    if(aux.isStopped){
                        despacharProcesso();
                        aux.isStopped = false;
                    }
                }else{
                    obj.tempoDuracao--;
                }
            }, 1000);
        }

        function removeObjAptos(obj) {
            angular.forEach(vm.filaDeAptos, function(v,k){
                if(v.name == obj.name){
                    vm.filaDeAptos.splice(k,1);
                }
            });
            vm.filaAbortados.push(obj);
        }

        function threadAptos(obj) {
            var isWithTimer = false;
            var timer = $interval(function(){
                if(!isWithTimer){
                    obj.timer = timer;
                    isWithTimer = true;
                }
                if(obj.estado == 3){
                    stopTimerProcesso(obj);
                }else{
                    if(obj.estado == 1) stopTimerProcesso(obj);
                    if(obj.estado != 1 && obj.deadLine == 0){
                        $interval.cancel(timer);
                        removeObjAptos(obj);
                    }else{
                        obj.deadLine--;
                    }
                }
            }, 1000);
        }


        function fillKeysCores(){
            for (var i = 0; i < vm.data.qtd_core; i++) {
                vm.cores.push(FactoryProcessador.novoProcessador());
            }
        }

        function run() {
            ordenarFilaAptos();
            fillKeysCores();
            angular.forEach(vm.filaDeAptos, function(value){
                threadAptos(value);
            });

        }

        function buildProcesso(){
            var novoProcesso = FactoryProcesso.novoProcessoLTG();
            novoProcesso.bg = "ui blue message";
            vm.filaDeAptos.push(novoProcesso);
            ordenarFilaAptos();
            threadAptos(novoProcesso);
        }

        function setCore() {
            for(var i =0; i<vm.data.qtd_core;i++){
                $scope.$emit("checkCores");
            }
        }

        vm.addNovoProcesso = function(){
            buildProcesso();
            $scope.$emit("checkCores");
        }

        setCore();

    }


})();