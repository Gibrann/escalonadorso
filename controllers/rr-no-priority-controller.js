(function () {

    "use strict";

    angular.module("app").controller("RRNoPriorityController", RRNoPriorityController);

    RRNoPriorityController.$inject = ["$scope","$stateParams", "$interval", "FactoryProcesso", "FactoryProcessador", "ServiceSemaforo"];

    function RRNoPriorityController($scope, $stateParams, $interval, FactoryProcesso, FactoryProcessador, ServiceSemaforo){

        var vm = this;
        vm.cores = [];
        vm.processosFinalizados = [];
        vm.listaProcessos = [];
        vm.disabled = true;

        if($stateParams.param){
            vm.data = $stateParams.param.data;
            fillListaProcesso();
            ServiceSemaforo.initialize();
            ServiceSemaforo.buildArrayOfKeys(vm.data.qtd_core);
            run();
        }
        
        function fillListaProcesso() {
            for(var i=0; i< vm.data.qtd_processo; i++){
                buildProcesso();
            }
        }

        function buildProcesso(isWithColor){
            var processo = {};
            isWithColor = arguments.length > 0 ? arguments[0] : false;
            processo = FactoryProcesso.novoProcessoRR();
            if(isWithColor)
                processo.bg = "ui blue message";
            processo.quantum = vm.data.quantum;
            vm.listaProcessos.push(processo);
        }

        function fillKeysCores(){
            for (var i = 0; i < vm.data.qtd_core; i++) {
                vm.cores.push(FactoryProcessador.novoProcessador());
            }
        }

        function setCore() {
            var i = 0;
            fillKeysCores();

            while(i < vm.data.qtd_core && !isListEmpty()) {
                var k = ServiceSemaforo.getAvailableKey();
                vm.cores[k].processo = vm.listaProcessos[0];
                vm.cores[k].cor = "green message";
                vm.listaProcessos.splice(0,1);
                i++;
            }
        }

        function getElementIndex(obj){
            var key = -1;
            angular.forEach(vm.cores, function(v,k){
                if(v.processo != undefined && obj.name === v.processo.name){
                    key = k;
                }
            });
            return key;
        }

        function realocarProcesso(obj) {
            obj.quantum = vm.data.quantum;
            var k = getElementIndex(obj);
            if(k != -1){
                vm.cores[k].processo = {name: ""};
                ServiceSemaforo.decrementAvailableCores();
                vm.cores[k].cor = "red message";
                ServiceSemaforo.pushKey(k);
            }
            vm.listaProcessos.push(obj);
        }

        function finalizarProcesso(obj) {
            var k = getElementIndex(obj);
            if(k != -1){
                vm.processosFinalizados.push(vm.cores[k].processo);
                vm.cores[k].processo = {name: ""};
                ServiceSemaforo.decrementAvailableCores();
                vm.cores[k].cor = "red message";
                ServiceSemaforo.pushKey(k);
            }
        }

        function isListEmpty() {
            var processos = vm.listaProcessos;
            return (processos.length == 0);
        }

        $scope.$on("escalonar", function () {
            if(!isListEmpty()){
                var key = ServiceSemaforo.getKeyCoresArray();
                if(key != undefined) {
                    vm.cores[key].processo = vm.listaProcessos[0];
                    vm.cores[key].cor = "green message";
                    buildThreadObj(key);
                    vm.listaProcessos.splice(0, 1);
                    ServiceSemaforo.incrementAvailableCores();
                }
            }
        });

        function buildThreadObj(key) {
            threadCores(vm.cores[key].processo);
        }

        function removeObjCore(obj){
            if(obj.tempoDuracao == 0){
                finalizarProcesso(obj);
            }else{
                realocarProcesso(obj);
            }
            $scope.$emit("escalonar");
        }

        function threadCores(obj) {
            var timer = $interval(function () {
                if(obj.quantum == 0 || obj.tempoDuracao == 0){
                    $interval.cancel(timer);
                    removeObjCore(obj);
                }else{
                    obj.tempoDuracao--;
                    obj.quantum--;
                }
            }, 1000);
        }

        //starta a simulação
        function run() {
            //preenche os cores
            setCore();

            //coloca os processos que estao dentro dos cores para a 'thread'
            angular.forEach(vm.cores, function(value){
                if(value.processo != undefined)
                    threadCores(value.processo);
            });

        }

        //Change quantum
        vm.changeQuantum = function(){
            vm.data.quantum = parseInt(vm.novoQuantum);
            vm.disabled = true;
        }


        vm.addNovoProcesso = function(){
            buildProcesso(true);
            $scope.$emit("escalonar");
        }

    };


})();