/**
 * Created by mac-gibran on 14/05/16.
 */
(function(){

    "use strict";

    angular.module("app").controller("IBSControllerQF", IBSControllerQF);

    IBSControllerQF.$inject = ["FactoryBestFit", "FactoryBlock", "$timeout", "$stateParams", "FactoryProcesso", "FilterManager", "SelectorInverval", "FactoryProcessador", "$interval", "Comparator", "$scope"];

    function IBSControllerQF(FactoryBestFit, FactoryBlock, $timeout, $stateParams, FactoryProcesso, FilterManager, SelectorInverval, FactoryProcessador, $interval, Comparator, $scope) {

        var vm = this;
        vm.clock = {};
        vm.clock.second = 0;
        vm.cores = [];
        vm.processosFinalizados = [];
        vm.processosAbortados = [];
        vm.labelRelogio = "Parar relógio";
        vm.memory = [];
        var aux = {};
        aux.keepGoing = true;
        aux.hasRemainders = true;
        aux.filaDaVez = -1;
        aux.isScheduleFree = true;
        aux.filaNovosProcessos = [];
        aux.busy = false;
        aux.allBlockCreated = false;
        aux.sumBlocks = 0;
        aux.currentSizeMemory = 0;

        if($stateParams.param){
            vm.data = $stateParams.param.data;
            vm.data.tamanho_memoria = parseInt(vm.data.tamanho_memoria);
            FactoryBlock.initialize();
            vm.cores = FactoryProcessador.buildProcessadoresIBS(vm.data.qtd_core);
            vm.processosIniciais = FactoryProcesso.buildProcessosIBS(vm.data.qtd_processo);
            fillListaCore();
        }

        function fillListaCore() {
            var i = 0;
            while(i < vm.data.qtd_core && aux.hasRemainders) {
                vm.cores[i].processos = getRunningProcessos();
                i++;
            }
            fillFilaEspera();
        }

        function getRunningProcessos() {
            FactoryProcesso.inverterInvervalos(vm.processosIniciais);
            vm.processosIniciais = FilterManager.orderByInicio(vm.processosIniciais);
            var resposta = SelectorInverval.selectRunningInterval(vm.processosIniciais);
            if (resposta[1].length == 0)
                aux.hasRemainders = false;
            vm.processosIniciais = resposta[1];
            return resposta[0];
        }

        function fillFilaEspera(){
            var j = 0;
            angular.forEach(vm.cores, function(core){
                angular.forEach(core.processos, function(p){
                    p.core = j;
                    runProcessosEspera(p);
                });
                j++;
            });
            fillListaRemanescentes();
        }

        function fillListaRemanescentes() {
            angular.forEach(vm.processosIniciais, function(v){
                runProcessosRemanescentes(v);
            });
        }

        function runProcessosEspera(obj) {
            var isWithTimer = false;
            var timer = $interval(function () {
                if(!isWithTimer){
                    obj.timer = timer;
                    isWithTimer = true;
                }
                if(obj.inicio <= vm.clock.second && vm.cores[obj.core].estado == 0){
                    $interval.cancel(timer);
                    vm.cores[obj.core].processos.splice(0,1);
                    escalonar(obj);
                }
            }, 1000);
        }

        function runProcessosRemanescentes(obj) {
            var isWithTimer = false;
            var timer = $interval(function () {
                if(!isWithTimer){
                    obj.timer = timer;
                    isWithTimer = true;
                }
                if (obj.inicio <= vm.clock.second) {
                    $interval.cancel(timer);
                    removeObjRemanescentes(obj);
                }
            }, 1000);
        }

        function removeObjRemanescentes(obj) {
            angular.forEach(vm.processosIniciais, function (v,k) {
                if(v.name == obj.name){
                    vm.processosIniciais.splice(k,1);
                }
            });

            vm.processosAbortados.push(obj);
        }

        function getFreeBlocks() {
            var freeBloks = [];
            var clone = FactoryBlock.getAllAvailableKeys();
            angular.forEach(clone, function (v) {
                freeBloks.push(vm.memory[v]);
            });
            freeBloks = FilterManager.orderBy(freeBloks, "size");
            return freeBloks;
        }

        function standartSchedule(obj) {
            var freeBlocks = [];
            if(!FactoryBlock.isEmpty() && !aux.busy){
                aux.busy = true;
                FactoryBestFit.initialize();
                freeBlocks = FactoryBestFit.findBestFit(getFreeBlocks(), obj.size, true);
                aux.busy = false;
            }
            if(freeBlocks && freeBlocks.length){
                angular.forEach(freeBlocks, function(v){
                    vm.memory[v.position].processo = obj;
                    vm.memory[v.position].estado = 1;
                    obj.block.push(v.position);
                });
                alocarProcessoCore(obj);
            }else{
                obj.estado = 3;
                vm.processosAbortados.push(obj);
                $scope.$emit("checkCores");
            }
        }

        function escalonar(obj){
            if(!aux.allBlockCreated)
                createBlock(obj);
            else
                standartSchedule(obj);
        }

        function createBlock(process) {
            var block = FactoryBlock.getNewBlock();
            aux.sumBlocks += process.size;
            if(vm.data.tamanho_memoria >= aux.sumBlocks){
                block.size = process.size;
                aux.currentSizeMemory = aux.sumBlocks;
                vm.memory.push(block);
                FactoryBlock.addKey(block.position);
                FactoryBlock.incrementAvailableKey();
                setBlockValues(block, process);
            }else{
                block.size = vm.data.tamanho_memoria - aux.currentSizeMemory;
                process.estado = 3;
                vm.processosAbortados.push(process);
                if(block.size > 0) {
                    vm.memory.push(block);
                    FactoryBlock.addKey(block.position);
                    FactoryBlock.incrementAvailableKey();
                }
                aux.allBlockCreated = true;
            }
        }

        function setBlockValues(block, process) {
            block.processo = process;
            block.estado = 1;
            block.bg = "ui green message";
            process.block.push(block.position);
            alocarProcessoCore(process);
        }

        function alocarProcessoCore(obj) {
            var index = obj.core;
            vm.cores[index].processo = obj;
            vm.cores[index].estado = 1;
            vm.cores[index].bg = "green message";
            angular.forEach(obj.block, function (v) {
                if(vm.memory[v]) {
                    vm.memory[v].bg = "ui green message";
                }
                FactoryBlock.removeKey(v);
            });
            runProcessoCore(vm.cores[index].processo);
        }

        function runProcessoCore(obj) {
            var timer = $interval(function () {
                if(obj.final == vm.clock.second || obj.tempoDuracao == 0){
                    $interval.cancel(timer);
                    removeObjCore(obj);
                }
                if(obj.tempoDuracao != 0){
                    obj.tempoDuracao--;
                }

            }, 1000);
        }

        function removeObjCore(obj){
            var index = obj.core;
            vm.cores[index].processo = {};
            vm.cores[index].estado = 0;
            vm.processosFinalizados.push(obj);
            vm.cores[index].bg = "red message";
            angular.forEach(obj.block, function (v) {
                vm.memory[v].estado = 0;
                vm.memory[v].bg = "ui red message";
                vm.memory[v].processo = {};
                FactoryBlock.addKey(v);
                FactoryBlock.incrementAvailableKey();
            });
        }

        /* clock */
        var tick = function() {
            if(aux.keepGoing){
                vm.clock.second++;
                $timeout(tick, 1000);
            }
        };
        $timeout(tick, 1000);


        vm.stopClock = function() {
            if(aux.keepGoing == false){
                vm.labelRelogio = "Parar relógio";
                aux.keepGoing = true;
                $timeout(tick, 1000);
            }else {
                vm.labelRelogio = "Iniciar relógio";
                aux.keepGoing = false;
            }
        }


        //ADICIONAR NOVO PROCESSO
        function copiarProcessosInicias() {
            var remainders = [];

            for(var i=0;i<vm.processosIniciais.length;i++){
                remainders.push(vm.processosIniciais[i]);
            }

            return remainders;
        }

        function retomarRelogioRemanescentes() {
            angular.forEach(vm.processosIniciais, function (p) {
                if(!p.timer) runProcessosRemanescentes(p);
            });
        }


        function retomarRelogioEspera(core) {
            angular.forEach(vm.cores[aux.filaDaVez].processos, function (p,k) {
                if(!p.timer){
                    p.core = core;
                    runProcessosEspera(p);
                }

                if(p.core != aux.filaDaVez){
                    vm.cores[aux.filaDaVez].processos.splice(k,1);
                }
            });
        }


        function trySchedule(remainders){
            var result = [];
            aux.filaDaVez++;
            if(remainders) var sobra = remainders;
            sobra = sobra.concat(vm.cores[aux.filaDaVez].processos);
            FactoryProcesso.inverterInvervalos(sobra);
            sobra = FilterManager.orderByInicio(sobra);
            result = SelectorInverval.selectRunningInterval(sobra);
            var re = Comparator.isListEquals(vm.processosIniciais, result[1]);
            if(!re && aux.filaDaVez < vm.data.qtd_core){
                sobra = result[1];
                trySchedule(sobra);
            }else{
                vm.cores[aux.filaDaVez].processos = result[0];
                vm.processosIniciais = result[1];
                retomarRelogioEspera(aux.filaDaVez);
                retomarRelogioRemanescentes();
                aux.filaDaVez = -1;
                aux.isScheduleFree = true;
                $scope.$emit("tryScheduleFree");

            }
        }

        $scope.$on("tryScheduleFree", function () {
            if(aux.filaNovosProcessos.length && aux.isScheduleFree){
                aux.isScheduleFree = false;
                var novoProcesso = aux.filaNovosProcessos[0];
                var remainders = [];
                remainders = copiarProcessosInicias();
                remainders.push(novoProcesso);
                aux.filaNovosProcessos.splice(0,1);
                trySchedule(remainders);
            }

        });

        vm.addNovoProcesso = function() {
            var novoProcesso = {};
            novoProcesso = FactoryProcesso.novoProcessoDinamicoIBS(vm.clock.second);
            aux.filaNovosProcessos.push(novoProcesso);
            $scope.$emit("tryScheduleFree");
        }

    }

})();
