(function(){

  "use strict";

  angular.module("app").controller("Main", Main);

  Main.$inject = ["$scope", "$state"];

  function Main($scope, $state){

        $scope.isRoundRobin = false;

        $scope.data = {};
      
        $scope.core_list = [];

        for(var i=0; i < 64; i++){
          $scope.core_list.push(i+1);
        }

        $scope.showQuantumInput = function(){
          if($scope.data.algoritmo == "round_robin" || $scope.data.algoritmo == "rr_no_priority"){
            $scope.isRoundRobin = true;
          }else{
            $scope.isRoundRobin = false;
          }
        }
      
        $scope.startAlgoritmo = function() {
            
          if($scope.data.algoritmo == "ltg") {
              if($scope.data.algoritmoMemoria == "bf"){
                  $state.go("ltg_best_fit", {param: {data: $scope.data}});
              }
              if($scope.data.algoritmoMemoria == "qf"){
                  $state.go("ltg_quick_fit", {param: {data: $scope.data}});
              }
              if($scope.data.algoritmoMemoria == "mf"){
                  $state.go("ltg_merge_fit", {param: {data: $scope.data}});
              }
          }

          if($scope.data.algoritmo == "round_robin") {
              if($scope.data.algoritmoMemoria == "bf"){
                  $state.go("round_robin_best_fit", {param: {data: $scope.data}});
              }
              if($scope.data.algoritmoMemoria == "qf"){
                  $state.go("round_robin_quick_fit", {param: {data: $scope.data}});
              }
              if($scope.data.algoritmoMemoria == "mf"){
                  $state.go("round_robin_merge_fit", {param: {data: $scope.data}});
              }
          }

          if($scope.data.algoritmo == "interval_scheduling") {
              if($scope.data.algoritmoMemoria == "bf"){
                  $state.go("interval_scheduling_best_fit", {param: {data: $scope.data}});
              }
              if($scope.data.algoritmoMemoria == "qf"){
                  $state.go("interval_scheduling_quick_fit", {param: {data: $scope.data}});
              }
              if($scope.data.algoritmoMemoria == "mf"){
                  $state.go("interval_scheduling_merge_fit", {param: {data: $scope.data}});
              }
          }

        }
  }

})();
