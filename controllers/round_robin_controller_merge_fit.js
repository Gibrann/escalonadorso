/**
 * Created by mac-gibran on 14/05/16.
 */
(function () {

    "use strict";

    angular.module("app").controller("RRControllerMF", RRControllerMF);

    RRControllerMF.$inject = ["Fila", "MergeFitService", "FactoryBlock", "$scope", "$stateParams", "$interval", "FactoryProcesso","CoreManager", "FactoryProcessador", "ServiceSemaforo"];

    function RRControllerMF(Fila, MergeFitService, FactoryBlock, $scope, $stateParams, $interval, FactoryProcesso,CoreManager, FactoryProcessador, ServiceSemaforo){

        var vm = this;
        vm.cores = [];
        vm.processosFinalizados = [];
        vm.processosAbortados = [];
        vm.listaPrioridade0 = [];
        vm.listaPrioridade1 = [];
        vm.listaPrioridade2 = [];
        vm.listaPrioridade3 = [];
        vm.memory = [];
        vm.disabled = true;
        var aux = {};
        aux.prioridades = [3,2,1,0];
        aux.daVez = 0;
        aux.sumBlocks = 0;
        aux.currentSizeMemory = 0;
        aux.allBlockCreated = false;
        aux.busy = false;
        aux.isStopped = true;

        if($stateParams.param){
            vm.data = $stateParams.param.data;
            ServiceSemaforo.initialize();
            ServiceSemaforo.buildArrayOfKeys(vm.data.qtd_core);
            vm.data.tamanho_memoria = parseInt(vm.data.tamanho_memoria);
            var block = FactoryBlock.getNewBlock();
            block.size = vm.data.tamanho_memoria;
            vm.memory.push(block);
            setListaPrioridade();
        }

        $scope.$on("checkCores", function(){
            if(CoreManager.exitsCoreEmpty(vm.data.qtd_core, ServiceSemaforo.getQtdUnavailableCores())){
                escalonar();
            }
        });

        function getQuantum(index){
            var quantum = parseInt(vm.data.quantum) + aux.prioridades[index];
            return quantum;
        }

        function buildProcesso(isWithColor){
            var processo = {};
            isWithColor = arguments.length > 0 ? arguments[0] : false;
            processo = FactoryProcesso.novoProcessoRR();
            if(isWithColor)
                processo.bg = "ui blue message";

            switch (processo.prioridade) {
                case 0:
                    processo.quantum = getQuantum(processo.prioridade);
                    vm.listaPrioridade0.push(processo);
                    break;
                case 1:
                    processo.quantum = getQuantum(processo.prioridade);
                    vm.listaPrioridade1.push(processo);
                    break;
                case 2:
                    processo.quantum = getQuantum(processo.prioridade);
                    vm.listaPrioridade2.push(processo);
                    break;
                case 3:
                    processo.quantum = vm.data.quantum;
                    vm.listaPrioridade3.push(processo);
                    break;
            }
        }

        function setListaPrioridade() {
            for (var i = 0; i < vm.data.qtd_processo; i++) {
                buildProcesso();
            }
        }

        function fillKeysCores(){
            for (var i = 0; i < vm.data.qtd_core; i++) {
                vm.cores.push(FactoryProcessador.novoProcessador());
            }
        }

        function setCore() {
            var i = 0;
            fillKeysCores();

            while(i < vm.data.qtd_core) {
                i++;
                $scope.$emit("checkCores");
            }
        }

        function getElementIndex(obj){
            var key = -1;
            angular.forEach(vm.cores, function(v,k){
                if(v.processo != undefined && obj.name === v.processo.name){
                    key = k;
                }
            });
            return key;
        }

        function todasListasVazia() {
            var lp0 = vm.listaPrioridade0;
            var lp1 = vm.listaPrioridade1;
            var lp2 = vm.listaPrioridade2;
            var lp3 = vm.listaPrioridade3;
            if(lp0.length == 0 && lp1.length == 0 && lp2.length == 0 && lp3.length == 0){
                return true;
            }else{
                return false;
            }
        }

        function stopTimerProcesso(processo) {
            if(processo.timer) $interval.cancel(processo.timer);
        }

        function alocarProcessoCore(k,obj) {
            if(k != undefined && obj) {
                ServiceSemaforo.removeKey(k);
                ServiceSemaforo.incrementUnavailableCores();
                stopTimerProcesso(obj);
                obj.estado = 1;
                vm.cores[k].processo = obj;
                vm.cores[k].cor = "green message";
                threadCores(obj);
            }
        }

        function standartSchedule(k, obj) {
            if(k == undefined) return;
            var result = MergeFitService.findBlockAvailabe(vm.memory, obj.size);
            if(result[0]){
                var index = result[2];
                ServiceSemaforo.removeKey(k);
                ServiceSemaforo.incrementUnavailableCores();
                obj.estado = 1;
                vm.memory[index].processo = obj;
                obj.block = index;
                obj.alocado = true;
                vm.cores[k].processo = obj;
                vm.cores[k].cor = "green message";
                threadCores(obj);
            }else{
                obj.estado = 3;
                vm.processosAbortados.push(obj);
                $scope.$emit("checkCores");
            }
        }

        function escalonar(){
            if(!todasListasVazia()){
                var key = undefined;
                var processo = undefined;
                switch(aux.daVez) {
                    case 0:
                        aux.daVez++;
                        if(vm.listaPrioridade0.length > 0){
                            key = ServiceSemaforo.getKeyCoresArray();
                            processo = vm.listaPrioridade0[0];
                            vm.listaPrioridade0.splice(0,1);
                        }
                        break;
                    case 1:
                        aux.daVez++;
                        if(vm.listaPrioridade1.length > 0){
                            key = ServiceSemaforo.getKeyCoresArray();
                            processo = vm.listaPrioridade1[0];
                            vm.listaPrioridade1.splice(0,1);
                        }
                        break;
                    case 2:
                        aux.daVez++;
                        if(vm.listaPrioridade2.length > 0){
                            key = ServiceSemaforo.getKeyCoresArray();
                            processo = vm.listaPrioridade2[0];
                            vm.listaPrioridade2.splice(0,1);
                        }
                        break;
                    case 3:
                        aux.daVez++;
                        if(vm.listaPrioridade3.length > 0){
                            key = ServiceSemaforo.getKeyCoresArray();
                            processo = vm.listaPrioridade3[0];
                            vm.listaPrioridade3.splice(0,1);
                        }
                        break;
                }

                if(aux.daVez >= 4){
                    aux.daVez = 0;
                }

                if(processo != undefined && !processo.alocado && key != undefined){
                    standartSchedule(key,processo);
                }else{
                    if(processo != undefined && processo.alocado && key != undefined){
                        alocarProcessoCore(key, processo);
                    }
                }

                $scope.$emit("checkCores");
            }

        }

        function finalizarProcesso(obj) {
            var k = getElementIndex(obj);
            if(k != -1){
                vm.processosFinalizados.push(vm.cores[k].processo);
                vm.cores[k].processo = {name: ""};
                vm.cores[k].cor = "red message";
                obj.alocado = false;
                ServiceSemaforo.pushKey(k);
                ServiceSemaforo.decrementUnavailableCores();
                var promiseDesalocar = MergeFitService.desalocarBloco(vm.memory, obj);
                promiseDesalocar.then(function () {
                    // var promiseMerge = MergeFitService.mergeFreeBlocks(vm.memory);
                    // promiseMerge.then(function () {
                        $scope.$emit("checkCores");
                    // }, function () {});
                }, function () {});
            }
        }

        function realocarProcesso(obj) {
            obj.quantum = getQuantum(obj.prioridade);
            var k = getElementIndex(obj);
            if(obj.alocado){
                if(k != -1){
                    vm.cores[k].processo = {name: ""};
                    vm.cores[k].cor = "red message";
                    ServiceSemaforo.pushKey(k);
                    ServiceSemaforo.decrementUnavailableCores();
                }
                switch (obj.prioridade) {
                    case 0:
                        vm.listaPrioridade0.push(obj);
                        break;
                    case 1:
                        vm.listaPrioridade1.push(obj);
                        break;
                    case 2:
                        vm.listaPrioridade2.push(obj);
                        break;
                    case 3:
                        vm.listaPrioridade3.push(obj);
                        break;
                }
            }else{
                vm.processosAbortados.push(obj);
            }
        }

        function removeObjCore(obj){
            if(obj.tempoDuracao == 0){
                finalizarProcesso(obj);
            }else{
                realocarProcesso(obj);
            }
            $scope.$emit("checkCores");
        }

        function despacharProcesso() {
            var timer = $interval(function () {
                if(Fila.isEmpty()){
                    $interval.cancel(timer);
                    aux.isStopped = true;
                }else {
                    var obj = Fila.desenfileirar();
                    removeObjCore(obj);
                }
            },100);
        }

        function threadCores(obj) {
            var timer = $interval(function () {
                if(obj.quantum == 0 || obj.tempoDuracao == 0){
                    $interval.cancel(timer);
                    Fila.enfileirar(obj);
                    if(aux.isStopped){
                        despacharProcesso();
                        aux.isStopped = false;
                    }
                }else{
                    obj.tempoDuracao--;
                    obj.quantum--;
                }
            }, 1000);
        }

        vm.changeQuantum = function(){
            vm.data.quantum = parseInt(vm.novoQuantum);
            vm.disabled = true;
        }
        
        function toMerge() {
            $interval(function () {
                var promise = MergeFitService.mergeFreeBlocks(vm.memory);
            },500);
        }


        vm.addNovoProcesso = function(){
            buildProcesso(true);
            $scope.$emit("checkCores");
        }

        setCore();

        toMerge();

    };


})();