
(function () {

    "use strict";

    angular.module("app").controller("LTGControllerQF", LTGControllerQF);

    LTGControllerQF.$inject = ["Fila","QuickFitService", "$scope", "$stateParams", "$interval", "CoreManager", "FilterManager", "FactoryProcesso", "FactoryProcessador", "ServiceSemaforo", "FactoryBlock"];

    function LTGControllerQF(Fila, QuickFitService, $scope, $stateParams, $interval, CoreManager, FilterManager, FactoryProcesso, FactoryProcessador, ServiceSemaforo, FactoryBlock) {


        var vm = this;
        vm.filaDeAptos = [];
        vm.cores = [];
        vm.filaAbortados = [];
        vm.processosFinalizados = [];
        vm.memory = [];
        vm.currentOftenBlocks = [];
        vm.isQuickFit = true;
        var aux = {};
        aux.sumBlocks = 0;
        vm.currentSizeMemory = 0;
        aux.allBlockCreated = false;
        aux.busy = false;

        if($stateParams.param){
            vm.data = $stateParams.param.data;
            vm.data.tamanho_memoria = parseInt(vm.data.tamanho_memoria);
            QuickFitService.initialize();
            FactoryBlock.initialize();
            ServiceSemaforo.initialize();
            ServiceSemaforo.buildArrayOfKeys(vm.data.qtd_core);
            setFilaAptos();
        }

        $scope.$on("checkCores", function (){
            if(CoreManager.exitsCoreEmpty(vm.data.qtd_core, ServiceSemaforo.getQtdUnavailableCores())){
                escalonar();
            }
        });

        function ordenarFilaAptos() {
            vm.filaDeAptos = FilterManager.orderByDeadLine(vm.filaDeAptos);
        }

        function setFilaAptos(){
            vm.filaDeAptos = FactoryProcesso.buildProcessosLTG(vm.data.qtd_processo);
            run();
        }

        function getElementIndex(obj){
            var key = -1;
            angular.forEach(vm.cores, function(v,k){
                if(v.processo != undefined && obj.name == v.processo.name){
                    key = k;
                }
            });
            return key;
        }

        function alocarProcessoCore(k,obj) {
            if(k != undefined && obj) {
                ServiceSemaforo.removeKey(k);
                ServiceSemaforo.incrementUnavailableCores();
                QuickFitService.countOftenBloks(vm.memory[obj.block], vm.memory);
                QuickFitService.incrementRequestCount();
                vm.currentOftenBlocks = QuickFitService.getCurrentOftenBlocks();
                var v = obj.block;
                if(vm.memory[v]) {
                    vm.memory[v].bg = "ui green message";
                }
                FactoryBlock.removeKey(v);
                obj.estado = 1;
                vm.cores[k].processo = obj;
                vm.cores[k].cor = "green message";
                stopTimerProcesso(obj);
                threadCores(obj);
            }
        }

        function stopTimerProcesso(processo) {
            if(processo.timer) $interval.cancel(processo.timer);
        }

        function standartSchedule(k, obj) {
            if(k == undefined) return;
            var freeBlock = undefined;
            if(!FactoryBlock.isEmpty() && !aux.busy){
                aux.busy = true;
                freeBlock = QuickFitService.findFreeBlock(getFreeBlocks(), obj, vm.memory);
                vm.currentOftenBlocks = QuickFitService.getCurrentOftenBlocks();
                aux.busy = false;
            }
            if(freeBlock != undefined){
                freeBlock.processo = obj;
                freeBlock.estado = 1;
                obj.block = freeBlock.position;
                alocarProcessoCore(k,obj);
            }else{
                if(!aux.allBlockCreated) {
                    createBlock(k, obj);
                }else{
                    obj.estado = 3;
                    vm.filaAbortados.push(obj);
                    $scope.$emit("checkCores");
                }
            }
        }

        function getFreeBlocks() {
            var freeBloks = [];
            var clone = FactoryBlock.getAllAvailableKeys();
            angular.forEach(clone, function (v) {
                freeBloks.push(vm.memory[v]);
            });
            freeBloks = FilterManager.orderBy(freeBloks, "position");
            return freeBloks;
        }

        function escalonar(){
            var k = ServiceSemaforo.getKeyCoresArrayLTG(vm.filaDeAptos.length > 0);
            var obj = getFirstApto();
            if(obj == undefined) return;
            vm.filaDeAptos.splice(0,1);
            stopTimerProcesso(obj);
            standartSchedule(k,obj);
        }

        function setBlockValues(k, block, process){
            block.processo = process;
            block.estado = 1;
            block.bg = "ui green message";
            process.block = block.position;
            alocarProcessoCore(k,process);
        }

        function createBlock(k, process) {
            var block = FactoryBlock.getNewBlock();
            aux.sumBlocks += process.size;
            if(vm.data.tamanho_memoria >= aux.sumBlocks){
                block.size = process.size;
                vm.currentSizeMemory = aux.sumBlocks;
                vm.memory.push(block);
                FactoryBlock.addKey(block.position);
                FactoryBlock.incrementAvailableKey();
                setBlockValues(k, block, process);
            }else{
                block.size = vm.data.tamanho_memoria - vm.currentSizeMemory;
                if(process.size >= block.size && block.size > 0) {
                    vm.memory.push(block);
                    vm.currentSizeMemory += block.size;
                    FactoryBlock.addKey(block.position);
                    FactoryBlock.incrementAvailableKey();
                    aux.allBlockCreated = true;
                    setBlockValues(k, block, process);
                }else{
                    process.estado = 3;
                    vm.filaAbortados.push(process);
                }

            }
        }

        function getFirstApto() {
            return vm.filaDeAptos[0];
        }

        function removeObjCore(obj){
            var k = getElementIndex(obj);
            if(k != -1){
                ServiceSemaforo.decrementUnavailableCores();
                vm.processosFinalizados.push(vm.cores[k].processo);
                vm.cores[k].processo = {};
                vm.cores[k].cor = "red message";
                var v = obj.block;
                vm.memory[v].estado = 0;
                if(vm.memory[v].often == true){
                    vm.memory[v].bg = "color" + vm.memory[v].size;
                }else{
                    vm.memory[v].bg = "ui red message";
                }
                vm.memory[v].processo = {};
                FactoryBlock.addKey(v);
                FactoryBlock.incrementAvailableKey();
                ServiceSemaforo.pushKey(k);
                $scope.$emit("checkCores");
            }
        }

        function despacharProcesso() {
            var timer = $interval(function () {
                if(Fila.isEmpty()){
                    $interval.cancel(timer);
                }else {
                    if(!aux.busy) {
                        var obj = Fila.desenfileirar();
                        removeObjCore(obj);
                    }
                }
            },100);
        }

        function threadCores(obj) {
            var timer = $interval(function () {
                if(obj.tempoDuracao == 0){
                    $interval.cancel(timer);
                    Fila.enfileirar(obj);
                    despacharProcesso();
                    // removeObjCore(obj);
                }else{
                    obj.tempoDuracao--;
                }
            }, 1000);
        }

        function removeObjAptos(obj) {
            angular.forEach(vm.filaDeAptos, function(v,k){
                if(v.name == obj.name){
                    vm.filaDeAptos.splice(k,1);
                }
            });
            vm.filaAbortados.push(obj);
        }

        function threadAptos(obj) {
            var isWithTimer = false;
            var timer = $interval(function(){
                if(!isWithTimer){
                    obj.timer = timer;
                    isWithTimer = true;
                }
                if(obj.estado == 3){
                    stopTimerProcesso(obj);
                }else{
                    if(obj.estado == 1) stopTimerProcesso(obj);
                    if(obj.estado != 1 && obj.deadLine == 0){
                        $interval.cancel(timer);
                        removeObjAptos(obj);
                    }else{
                        obj.deadLine--;
                    }
                }
            }, 1000);
        }


        function fillKeysCores(){
            for (var i = 0; i < vm.data.qtd_core; i++) {
                vm.cores.push(FactoryProcessador.novoProcessador());
            }
        }

        function run() {
            ordenarFilaAptos();
            fillKeysCores();

            angular.forEach(vm.filaDeAptos, function(value){
                threadAptos(value);
            });

        }

        function buildProcesso(){
            var novoProcesso = FactoryProcesso.novoProcessoLTG();
            novoProcesso.bg = "ui blue message";
            vm.filaDeAptos.push(novoProcesso);
            ordenarFilaAptos();
            threadAptos(novoProcesso);
        }

        function setCore() {
            for(var i =0; i<vm.data.qtd_core;i++){
                $scope.$emit("checkCores");
            }
        }

        vm.addNovoProcesso = function(){
            buildProcesso();
            $scope.$emit("checkCores");
        }

        setCore();

    }


})();